import { Component, OnInit, Input } from '@angular/core';
import { Poll } from '../types/poll';
import { Vote } from '../types/vote';
import { generateVoteString } from '../utils';

@Component({
  selector: 'app-poll-detail',
  templateUrl: './poll-detail.component.html',
  styleUrls: ['./poll-detail.component.css']
})
export class PollDetailComponent implements OnInit {

  @Input() poll: Poll;

  constructor() { }

  generateVoteString(vote: Vote) {
    return generateVoteString(vote.vote);
  }

  ngOnInit() {
  }

}
