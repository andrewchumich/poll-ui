import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AddPollDialogComponent } from '../add-poll-dialog/add-poll-dialog.component';
import { CreatePollGroup } from '../types/poll';

@Component({
  selector: 'app-group-polls-dialog',
  templateUrl: './group-polls-dialog.component.html',
  styleUrls: ['./group-polls-dialog.component.css']
})
export class GroupPollsDialogComponent implements OnInit {

  public onAdd: EventEmitter<CreatePollGroup> = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<AddPollDialogComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: { polls: string[] }
  ) {
  }

  public groupPollsForm = this.fb.group({
    name: new FormControl('', [
      Validators.required,
    ]),
  });

  ngOnInit() {
  }

  submitForm(formValue: FormGroup) {
    this.onAdd.emit({
      name: formValue.value.name,
      polls: this.data.polls,
    });
  }
}
