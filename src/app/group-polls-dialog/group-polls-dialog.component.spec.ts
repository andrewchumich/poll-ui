import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupPollsDialogComponent } from './group-polls-dialog.component';

describe('GroupPollsDialogComponent', () => {
  let component: GroupPollsDialogComponent;
  let fixture: ComponentFixture<GroupPollsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupPollsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupPollsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
