import { Vote } from "./vote";

export interface CreatePoll {
  name: string;
  options: string[];
  type: 'SchulzeSTV';
  winners: number;
}

export interface CreatePollGroup {
  name: string;
  polls: string[];
}

export interface Poll extends CreatePoll {
  id: string;
  votes: Vote[];
  results: Results | null;
  locked: boolean;
}

export interface PollGroup {
  id: string;
  name: string;
  polls: Poll[];
}

export interface Results {
  candidates: string[];
  winners: string[];
  tie_breaker?: string[];
  tied_winners?: string[];
  actions?: string;
}
