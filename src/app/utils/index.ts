import { Vote } from "../types/vote";

export function generateVoteString(vote: string[][]): string {
  return vote.reduce((voteString, voteLevel, index) => {
    if (index > 0 && voteLevel.length > 0) {
      voteString += ' > ';
    }

    return voteLevel.reduce((vs, option, i) => {
      if (i > 0 && voteLevel.length > 1) {
        vs += ' = ';
      }
      vs += option;
      return vs;
    }, voteString);
  }, '');
}
