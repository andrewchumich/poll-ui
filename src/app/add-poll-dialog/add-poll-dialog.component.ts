import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormControl, FormBuilder, FormArray, Validators, AbstractControl, FormGroup } from '@angular/forms';
import { CreatePoll } from '../types/poll';

function minLengthArray(min: number) {
  return (c: AbstractControl): {[key: string]: any} => {
      if (c.value.length >= min)
          return null;

      return { 'minLengthArray': { valid: false }};
  }
}
@Component({
  selector: 'app-add-poll-dialog',
  templateUrl: './add-poll-dialog.component.html',
  styleUrls: ['./add-poll-dialog.component.css']
})
export class AddPollDialogComponent implements OnInit {
  public onAdd: EventEmitter<CreatePoll> = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<AddPollDialogComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  public pollForm = this.fb.group({
    name: new FormControl('', [
      Validators.required,
    ]),
    winners: new FormControl(1, [
      Validators.required,
    ]),
    options: this.fb.array([
      new FormControl(''),
    ], minLengthArray(2)),
    type: new FormControl('ShulzeSTV'),
  });

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submitForm(formValue: FormGroup) {
    this.onAdd.emit(formValue.value);
  }

  addOption() {
    let options = <FormArray>this.pollForm.get('options');
    options.push(new FormControl(''));
  }

  removeOption() {
    let options = <FormArray>this.pollForm.get('options');
    options.removeAt(options.length - 1);
  }

}
