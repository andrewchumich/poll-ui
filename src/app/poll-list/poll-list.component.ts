import { Component, OnInit, OnDestroy } from '@angular/core';
import { Poll } from '../types/poll';
import { Subject } from 'rxjs';
import { PollService } from '../poll/poll.service';
import { takeUntil } from 'rxjs/operators';
import { generateVoteString } from '../utils';

@Component({
  selector: 'app-poll-list',
  templateUrl: './poll-list.component.html',
  styleUrls: ['./poll-list.component.css']
})
export class PollListComponent implements OnInit, OnDestroy {

  public polls: Poll[];
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private pollService: PollService,
  ) { }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngOnInit() {
    this.refreshData();
  }

  refreshData() {
    this.pollService
      .getPolls()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(polls => {
        this.polls = polls;
      });
  }


}
