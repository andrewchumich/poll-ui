import { Component, OnInit, OnDestroy } from '@angular/core';
import { PollService } from '../poll/poll.service';
import { Poll } from '../types/poll';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { Vote } from '../types/vote';

interface Params {
  id: string;
}

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})
export class VoteComponent implements OnInit, OnDestroy {

  public poll: Poll;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  public params: Params;
  public voteLevels: Set<string>[] = [new Set()];
  public selectedVoteLevelIndex: number = 0;
  public hasVoted: boolean = false;
  public submittedVote: Vote;

  constructor(
    private pollService: PollService,
    private route: ActivatedRoute,
    public snackBar: MatSnackBar,
  ) { }

  vote(level: number, vote: string) {
    if (this.voteLevels.length <= level) {
      return;
    }

    let should_delete_option = false;
    let selectedLevelSet = this.voteLevels[level];
    if (selectedLevelSet.has(vote)) {
      should_delete_option = true;
    }
    selectedLevelSet.add(vote);

    let newVoteLevels: Set<string>[] = [];
    let newSelectedLevel = level;
    this.voteLevels.forEach((levelSet, i) => {
      if (i !== level || (i === level && should_delete_option)) {
        levelSet.delete(vote);
      }

      newVoteLevels.push(levelSet);
    });

    if (newVoteLevels.length === 0) {
      newVoteLevels.push(new Set());
    }

    if (newVoteLevels[newVoteLevels.length - 1].size > 0) {
      newVoteLevels.push(new Set());
    }

    this.selectedVoteLevelIndex = newSelectedLevel;
    this.voteLevels = newVoteLevels;

  }

  isChecked(option: string, selectedVoteLevelIndex: number, voteLevels: Set<string>[]): boolean {
    if (selectedVoteLevelIndex >= voteLevels.length) {
      return false;
    }
    const voteLevel = voteLevels[selectedVoteLevelIndex];
    return voteLevel.has(option);
  }

  onChangeVoteLevel(voteLevel: number) {
    if (this.voteLevels.length <= voteLevel) {
      return;
    }
    this.selectedVoteLevelIndex = voteLevel;
  }

  submitVote(voteLevels: Set<string>[]) {
    if (voteLevels.length === 0 || voteLevels[0].size === 0) {
      console.warn('Need to have at least one vote');
      return;
    }
    const voteArray = voteLevels.map(voteLevel => Array.from(voteLevel));
    this.hasVoted = true;
    this.pollService
      .vote(this.params.id, voteArray)
      .subscribe(result => {
        console.log(result);
        if (result.hasOwnProperty(this.params.id)) {
          if (result[this.params.id] === null) {
            this.snackBar.open('Vote failed', '', {
              duration: 1500,
            });
            this.hasVoted = false;
            return;
          }
          this.submittedVote = result[this.params.id];
        }
        this.snackBar.open('Successfully voted!', '', {
          duration: 1500,
        });
      }, err => {
        this.hasVoted = false;
        this.snackBar.open('Vote failed', '', {
          duration: 1500,
        });
      });
  }

  ngOnInit() {
    this
      .route
      .params
      .pipe(
        takeUntil(this.destroy$),
      )
      .subscribe((params: Params) => {
        this.params = params;
        if (this.params.hasOwnProperty('id') === false) {
          return;
        }
        this.pollService
          .getPoll(this.params.id)
          .pipe(
            takeUntil(this.destroy$)
          )
          .subscribe(poll => {
            if (poll.options.length < 1) {
              return;
            }
            this.poll = poll
          })
          ;
      });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
