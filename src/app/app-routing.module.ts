import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PollManagerComponent } from './poll-manager/poll-manager.component';
import { PollListComponent } from './poll-list/poll-list.component';
import { VoteComponent } from './vote/vote.component';

const routes: Routes = [
  { path: '', redirectTo: '/poll-list', pathMatch: 'full' },
  { path: 'poll-manager', component: PollManagerComponent },
  { path: 'poll-list', component: PollListComponent },
  { path: 'vote/:id', component: VoteComponent },
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
