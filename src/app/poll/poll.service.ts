import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, of } from 'rxjs';
import { Poll, Results, CreatePoll, CreatePollGroup } from '../types/poll';
import { tap, map, catchError } from 'rxjs/operators';
import { Vote } from '../types/vote';

@Injectable({
  providedIn: 'root'
})
export class PollService {

  loading: boolean = false;

  constructor(private http: HttpClient) {}

  getPolls(): Observable<Poll[]> {
    this.loading = true;
    return this.http
      .get<Poll[]>(`${environment.apiRoot}/poll`)
      .pipe(
        tap(polls => {
          console.log('Fetched polls', polls)
          this.loading = false;
        })
      )
  }

  getPoll(id: string): Observable<Poll> {
    this.loading = true;
    return this.http
      .get<Poll>(`${environment.apiRoot}/poll/${id}`)
      .pipe(
        tap(poll => {
          console.log('Fetched polls', poll)
          this.loading = false;
        })
      )
  }

  compute(id: string): Observable<Results> {
    this.loading = true;
    return this.http
      .get<Results>(`${environment.apiRoot}/compute/${id}`)
      .pipe(
        tap(results => {
          console.log('Results:', results)
          this.loading = false;
        })
      );
  }

  lock(poll_id: string, locked: boolean): Observable<boolean | null> {
    return this.http
      .post<boolean>(`${environment.apiRoot}/lock/${poll_id}`, {
        locked,
      })
      .pipe(
        tap(response => {
          if (response === true) {
            console.log(`Lock successful: ${poll_id}`);
          } else {
            console.warn(`Lock failed: ${poll_id}`);
          }
        }),
        catchError(error => {
          console.warn(`Error deleting poll: ${poll_id}`, error);
          return of(null);
        })
      )
  }

  addPoll(poll: CreatePoll): Observable<boolean> {
    this.loading = true;
    return this.http
      .post<any>(`${environment.apiRoot}/poll`, poll)
      .pipe(
        map(addPollResult => {
          console.log('Add poll:', addPollResult);
          this.loading = false;
          return true;
        }),
        catchError(error => {
          console.log(`Error adding poll: ${error}`);
          this.loading = false;
          return of(false);
        })
      )
  }

  groupPolls(create_poll_group: CreatePollGroup): Observable<boolean> {
    this.loading = true;
    return this.http
      .post<any>(`${environment.apiRoot}/group-polls`, create_poll_group)
      .pipe(
        map(groupPollsResult => {
          console.log('Group Polls:', groupPollsResult);
          this.loading = false;
          return true;
        }),
        catchError(error => {
          console.log(`Error adding poll: ${error}`);
          this.loading = false;
          return of(false);
        })
      )
  }

  deletePoll(poll_id: string): Observable<boolean> {
    this.loading = true;
    return this.http
      .delete<boolean>(`${environment.apiRoot}/poll/${poll_id}`)
      .pipe(
        tap(response => {
          if (response === true) {
            console.log(`Delete successful: ${poll_id}`);
          } else {
            console.warn(`Delete failed: ${poll_id}`);
          }
        }),
        catchError(error => {
          console.warn(`Error deleting poll: ${poll_id}`, error);
          return of(false);
        })
      )
  }

  vote(poll_id: string, votes: string[][]): Observable<{ [poll_id: string]: Vote }> {
    this.loading = true;
    return this.http
      .post<{ [poll_id: string]: Vote }>(`${environment.apiRoot}/vote`, {
        vote: {
          [poll_id]: votes,
        }
      })
  }
}
