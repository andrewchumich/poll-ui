import { Component, OnInit, OnDestroy } from '@angular/core';
import { PollService } from '../poll/poll.service';
import { Poll, CreatePoll, CreatePollGroup } from '../types/poll';
import {MatDialog, MatSnackBar} from '@angular/material';
import { AddPollDialogComponent } from '../add-poll-dialog/add-poll-dialog.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GroupPollsDialogComponent } from '../group-polls-dialog/group-polls-dialog.component';

@Component({
  selector: 'app-poll-manager',
  templateUrl: './poll-manager.component.html',
  styleUrls: ['./poll-manager.component.css'],
})
export class PollManagerComponent implements OnInit, OnDestroy {

  public polls: Poll[];
  public selectedPolls: Set<string> = new Set();
  public addDialogOpen: boolean = false;
  public groupPollsOpen: boolean = false;
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private pollService: PollService,
    private dialog: MatDialog,
    public snackBar: MatSnackBar,
  ) { }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngOnInit() {
    this.refreshData();
  }

  isPollSelected(poll: Poll) {
    return this.selectedPolls.has(poll.id);
  }

  selectPoll(e: PointerEvent, poll: Poll) {
    e.stopPropagation();
    e.preventDefault();
    if (this.selectedPolls.has(poll.id)) {
      this.selectedPolls.delete(poll.id);
    } else {
      this.selectedPolls.add(poll.id);
    }
  }

  refreshData() {
    this.pollService
      .getPolls()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(polls => {
        this.polls = polls;
      });
  }

  tallyVote(poll: Poll) {
    this.pollService
      .compute(poll.id)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(results => {
        this.refreshData();
      });
  }

  deletePoll(poll: Poll) {
    const confirm = window.confirm('Are you sure you want to delete this poll?');
    if (confirm === false) {
      return;
    }

    this.pollService
      .deletePoll(poll.id)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(response => {
        if (response === true) {
          this.refreshData();
          this.snackBar.open('Delete success', '', {
            duration: 1500,
          });
        } else {
          this.snackBar.open('Delete failed', '', {
            duration: 1500,
          });
        }
      });
  }

  setLock(e: MouseEvent, poll_id: string, locked: boolean) {
    e.preventDefault();
    e.stopPropagation();
    this.pollService
      .lock(poll_id, locked)
      .subscribe(res => {
        this.refreshData();
      })
  }

  groupPolls() {
    const dialogRef = this.dialog.open(GroupPollsDialogComponent, {
      width: '550px',
      data: {
        polls: Array.from(this.selectedPolls),
      }
    });

    dialogRef.afterClosed().subscribe((poll_group: CreatePollGroup) => {
      console.log('The dialog was closed');
      console.log(poll_group);
    });

    const sub = dialogRef.componentInstance.onAdd.subscribe((poll_group: CreatePollGroup) => {
      console.log(poll_group);
      this.pollService
        .groupPolls(poll_group)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(result => {
          if (result === true) {
            dialogRef.close();
            this.snackBar.open('Successfully grouped polls', '', {
              duration: 1500,
            });
            this.refreshData();
          } else {
            this.snackBar.open('Group polls failed', '', {
              duration: 1500,
            });
          }
        });
    });
    dialogRef.afterClosed().subscribe(() => {
      // unsubscribe onAdd
      sub.unsubscribe();
    });
  }

  addPoll() {
    const dialogRef = this.dialog.open(AddPollDialogComponent, {
      width: '550px',
      data: {
        name,
      }
    });

    dialogRef.afterClosed().subscribe((poll: Poll) => {
      console.log('The dialog was closed');
      console.log(poll);
    });

    const sub = dialogRef.componentInstance.onAdd.subscribe((poll: CreatePoll) => {
      this.pollService
        .addPoll(poll)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(result => {
          if (result === true) {
            dialogRef.close();
            this.snackBar.open('Successfully added poll', '', {
              duration: 1500,
            });
            this.refreshData();
          } else {
            this.snackBar.open('Add poll failed', '', {
              duration: 1500,
            });
          }
        });
    });
    dialogRef.afterClosed().subscribe(() => {
      // unsubscribe onAdd
      sub.unsubscribe();
    });
  }

}
