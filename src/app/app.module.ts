import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { PollManagerComponent } from './poll-manager/poll-manager.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './custom-material/custom-material.module';
import { AddPollDialogComponent } from './add-poll-dialog/add-poll-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PollDetailComponent } from './poll-detail/poll-detail.component';
import { PollListComponent } from './poll-list/poll-list.component';
import { VoteComponent } from './vote/vote.component';
import { GroupPollsDialogComponent } from './group-polls-dialog/group-polls-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    PollManagerComponent,
    AddPollDialogComponent,
    PollDetailComponent,
    PollListComponent,
    VoteComponent,
    GroupPollsDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    ReactiveFormsModule,
  ],
  providers: [
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AddPollDialogComponent,
    GroupPollsDialogComponent,
  ]
})
export class AppModule { }
